title: Now we wait
category: progress
tags: v3
---

Version 3.0 has been submitted to the Unity Asset Store team. It has been quite
a wild ride, but now it looks like we are good to go. Now all that's left is to
sit back, wait for a response and enjoy the calm. See you one the other side :)
